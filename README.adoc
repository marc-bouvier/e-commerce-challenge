= e-commerce-challenge

From https://www.linkedin.com/posts/sylvain-bastid-9b889249_developpeurs-juniors-recruteurs-activity-6778618348956651520-HaTF/[Sylvain Bastid] Linkedin post.

== Where to start

* https://marc-bouvier.gitlab.io/e-commerce-challenge/e-commerce-challenge[Live documentation]

== Contraintes que je m'impose

Contraintes impératives

* TDD
* ATTD
* Clean architecture
* ADR
* Contribuer a la cagnotte

Nice to have

* Externalisation de l'access control
* Externalisation de l'authentification (indielogin?)
* Front-end accessible (a11y)
* Éco-conception

Cool to have

* gdpr & data privacy
* mostly static site generation

== contexte

Salut à tous les #developpeurs #juniors !
Pour bien commencer la semaine, j'ai une offre pour vous.
🤑 Je vous propose 50€ pour faire un site e-commerce !

"Quoi ? 50€ ? pour un site e-commerce ? Tu nous prendrais pas un peu pour des jambons Sylvain ?🤬"

Attendez avant de m'insulter ^^
Parmi les conseils récurrents que je donne, le plus fréquent est : "Ayez des réalisations à montrer" et dès fois les projets persos.. on a tendance à remettre ça à plus tard.
🤟Du coup j'ai décidé de vous donner un petit coup de motivation en offrant 50€ à la personne qui aura fait le site-e-commerce le plus complet et bien entendu le site vous appartiendra à VOUS, pour vous, rien que pour vous, je n'en ferai aucun usage, ni commercial, ni personnel.

Donc en résumé, vous faite VOTRE réalisation perso et je vous offre 50€, sympa non ?
50€ c'est pas énorme on est d'accord mais c'est sur mes sous perso et vous vous doutez que je ne suis pas millionnaire😄.
(A tous les #recruteurs, #mentors, #devs seniors impliqués dans le coaching des juniors, si vous voulez augmenter la cagnotte, contactez moi, 100% de vos dons seront reversés.)

Bien sûr les bons projets seront mis en avant pour aider à votre recherche d'emploi.

Les règles en commentaire et contactez moi si vous avez des questions :) 👇

== règles

Règles du jeu :

*  🤜 Vous ne devez pas être en poste, être en recherche d'emploi et ne pas avoir d'expérience pro (hors stages).
*  📅Vous avez jusqu'au 20 Avril (si d'ici là personne n'a rendu un projet assez complet, on pourra prolonger)
*  💪Faire un site e-commerce le plus complet possible (login/inscription, fiche et listing produits, commandes, livraison/paiement (fake), backoffice, stats, etc... )
*  🤩Langages et frameworks autorisés : Php, symfony, laravel, javascript natif, node, react, angular, vue, mysql/mariadb
*  😱Doit être référençable (SEO), donc si framework js prévoir de générer des pages statiques
*  🤠Je dois pouvoir tester le site facilement, donc soit vous l'hébergez quelque part, soit prévoir une commande pour que je puisse le déployer facilement sur un wamp et ne pas passer 3 jours à configurer un serveur ^^
*  🤗Il doit y avoir un repo GIT (gitlab ou github) pour pouvoir voir la qualité de code.
*  🤯Seront jugées les fonctionnalités, la qualité du code, les performances (cache, index, optimisations..)
*  💩Bien entendu tous les CMS et modules tout prêts sont interdits.
*  🥳Sont autorisés les libs classiques du genre bootstrap, materialdesign, moment.js, jquery, datatables.. et tous les trucs de ce genre qui facilitent le dev.
