# Use Asciidoc for documentation

- Status: accepted
- Date: 2021-03-24
- Tags: dev-tools, doc 

## Context and Problem Statement

We want to document relevant informations for this project.
Which tool(s) should we use to document these resources?

## Considered Options

- [Markdown](https://www.markdownguide.org/)
- [LateX](https://www.latex-project.org/)
- [Asciidoctor](https://asciidoctor.org/)
- [LibreOffice](https://www.libreoffice.org/) / [Word](https://products.office.com/en-us/word)

## Decision Outcome

Chosen option: "Asciidoctor", because 
- Is easy to setup and start with
- Out of the box support for site generation
- Admonitions
- Tables are easy to edit
- Supports file inclusion
- Supports snippets from files
- Human readable in plain text
- Has no esoteric "flavors"
- Supported out of the box by Gitlab and Github
- Footnote support
- IDE support

### Positive Consequences

- Documentation generation can be versionned and reviewed easily as it is plain text
- Documentation can be generated in CI pipeline

## Links

- [Asciidoctor Documentation Home](https://docs.asciidoctor.org/home/)
