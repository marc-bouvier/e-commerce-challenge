# Use Java 11 for backend

- Status: superseded by [20210326-use-maven-to-build-java-sources](20210326-use-maven-to-build-java-sources.md)
- Date: 2021-03-26
- Tags: backend, java

## Context and Problem Statement

We want to write a backend to serve data and compute business logic.
Which language to use for writing this backend?

## Considered Options

- Java 11
- Kotlin

## Decision Outcome

Chosen option: "Java", because I am familiar with this language and I don't want to add the hassle to learn another language for this part.
Also, I am familiar with java tooling.
Version of Java used will be 11 because it is LTS at the moment I took this decision.

### Positive Consequences

- Better code quality
- Quicker progression
- Since the challenge ask for PHP or NodeJs curious contestants will be able to compare PHP / NodeJs solution with Java

### Negative Consequences

- Since the challenge ask for PHP or NodeJs backend most of the contestants won't be able to understand this code easily
