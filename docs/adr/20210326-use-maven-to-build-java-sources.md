# Use Maven to build java sources

- Status: accepted
- Date: 2021-03-26
- Tags: java, build, CI, packaging

## Context and Problem Statement

We need to manage dependencies and compilation for Java source code.
Which tool to build and test java source code?


## Considered Options

- Maven
- Gradle

## Decision Outcome

Chosen option: "Maven", because I am used to this tool. It is a pretty standard tool.

### Positive Consequences

- No additional learning and hassle for building and testing Java source code

### Negative Consequences

N/A
