all: build-all

# Echoes tooling installation
full-setup: setup-intro	adr-setup doc-setup	sdkman-setup java-setup graphviz-setup vscode-cml-extension-setup

build-all:
	mvn clean install
	antora docs/antora-playbook.yml

adr-new:
	log4brains adr new
adr-generate:
	log4brains build
adr-preview:
	log4brains preview

setup-intro:
	@echo "These instructions will allow you to install this project's tooling."
	@echo For "npm -g" commands, you may need to run it with sudo depending your npm modules configuration.
sdkman-setup:
	@echo
	@echo To install Sdkman Cli please run the following command :
	@echo
	@echo "    curl -s "https://get.sdkman.io" | bash"
adr-setup:
	@echo
	@echo To install ADR with log4brains Cli please run the following command :
	@echo 
	@echo "    npm install -g log4brains"
doc-setup:
	@echo
	@echo To install Antora Asciidoc site generator, please run the following command :
	@echo
	@echo "    npm i -g @antora/cli@2.3 @antora/site-generator-default@2.3"
java-setup: 
	@echo
	@echo To install java with sdkman you may run the following command :
	@echo
	@echo "    sdk install java 11.0.10.j9-adpt"
graphviz-setup:
	@echo Install Graphviz with the following instructions : https://www.graphviz.org/download/
	@echo Or use a package manager
	@echo
	@echo "    sudo apt-get install graphviz"
maven-setup:
	@echo To install Maven run the following command :
	@echo
	@echo "    sdk install maven"

plantuml-setup:
	@echo 
	@echo Install PlantUml with the following instructions : 
	@echo Or use a package manager
	@echo
	@echo "    sudo apt-get install plantuml"
	@echo
	@echo Sometimes, plantuml jar installed with package manager is quite old.
	@echo The command which shows where the executable shell script is installed
	@echo 
	@echo "    which plantuml"
	@echo "    /usr/bin/plantuml"
	@echo 
	@echo By looking inside this script, we can see that the plantuml jar is located at /usr/share/plantuml/plantuml.jar
	@echo If we want to install a newer version of plantuml, we can overwrite this plantuml.jar file.
	@echo We can also use a symbolic link to point to a plantuml.jar in another location.
vscode-cml-extension-setup:
	@echo 
	@echo To install Context Mapper Vs Code extension, run the following command :
	@echo 
	@echo "    code --install-extension contextmapper.context-mapper-vscode-extension"